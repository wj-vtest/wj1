package demo.co.testScripts;

import java.io.FileNotFoundException;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.agiletestware.pangolin.annotations.Pangolin;
import demo.co.Base.BaseClass;
import demo.co.Base.CommonFunctions;
import demo.co.Config.Constants;
import demo.co.pageObjects.ChannelsPageFactory;
import demo.co.pageObjects.HomePage;
import demo.co.pageObjects.LoginPage;

/**
 * @author Ranjit
 */

@Pangolin(sectionPath = "Master\\Section\\WebDriver")
public class ChannelsTestSuite extends BaseClass {

	public ChannelsTestSuite() {
		super();
	}
	
	

	@Test(priority = 1)
	public void EmpCanPostC10() throws FileNotFoundException, InterruptedException {
		LoginPage loginPage = new LoginPage();
		String randomString = CommonFunctions.generateRandomString(8);
		loginPage.getLoginFunctionality(prop.getProperty("Employee1UserName"), prop.getProperty("Employee1Password"));
		HomePage homePage = new HomePage();
		Thread.sleep(5000);
		homePage.buttonSideMenu.click();
		CommonFunctions.selectElementUsingText(Constants.CHANNELS, getDriver()).click();
		Thread.sleep(6000);
		ChannelsPageFactory channelsPage = new ChannelsPageFactory();
		CommonFunctions.waitForElementVisible(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]",
				getDriver());
		channelsPage.buttonChannel.click();
		CommonFunctions.waitForElementVisible(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ImageButton",
				getDriver());
		channelsPage.buttonPencil.click();
		CommonFunctions.waitForElementVisible(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.EditText",
				getDriver());
		channelsPage.textAreaEditPost.sendKeys(randomString);
		channelsPage.buttonSend.click();
		Thread.sleep(5000);
		CommonFunctions.scrollToElementUsingText(getDriver(), randomString);
	}

	@Test(priority = 2)
	public void EmpCanDeleteC11() throws FileNotFoundException, InterruptedException {
		LoginPage loginPage = new LoginPage();
		String randomString = CommonFunctions.generateRandomString(8);
		loginPage.getLoginFunctionality(prop.getProperty("Employee1UserName"), prop.getProperty("Employee1Password"));
		HomePage homePage = new HomePage();
		Thread.sleep(5000);
		homePage.buttonSideMenu.click();
		CommonFunctions.selectElementUsingText(Constants.CHANNELS, getDriver()).click();
		Thread.sleep(6000);
		ChannelsPageFactory channelsPage = new ChannelsPageFactory();
		CommonFunctions.waitForElementVisible(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]",
				getDriver());
		channelsPage.buttonChannel.click();
		CommonFunctions.waitForElementVisible(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ImageButton",
				getDriver());
		channelsPage.buttonPencil.click();
		CommonFunctions.waitForElementVisible(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.EditText",
				getDriver());
		channelsPage.textAreaEditPost.sendKeys(randomString);
		channelsPage.buttonSend.click();
		Thread.sleep(5000);
		CommonFunctions.scrollToElementUsingText(getDriver(), randomString);
		 Assert.assertEquals(channelsPage.textPostNames.getText(),
		 randomString);
		channelsPage.buttonPostEditMenus.click();
		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, getDriver()).getText(),
				Constants.DELETE_POST);
		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.EDIT_POST, getDriver()).getText(),
				Constants.EDIT_POST);
		System.out.println(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, getDriver()).isEnabled());
		CommonFunctions.selectElementUsingText(Constants.DELETE_POST, getDriver()).click();
		Assert.assertNotEquals(channelsPage.textPostNames.getText(), randomString);

	}

	@Test(priority = 3)
	public void EmpCanEditC12() throws FileNotFoundException, InterruptedException {
	LoginPage loginPage = new LoginPage();
	String randomString = CommonFunctions.generateRandomString(8);
	String randomString1 = CommonFunctions.generateRandomString(10);
	loginPage.getLoginFunctionality(prop.getProperty("Employee1UserName"), prop.getProperty("Employee1Password"));
	HomePage homePage = new HomePage();
	Thread.sleep(5000);
	homePage.buttonSideMenu.click();
	CommonFunctions.selectElementUsingText(Constants.CHANNELS, getDriver()).click();
	Thread.sleep(6000);
	ChannelsPageFactory channelsPage = new ChannelsPageFactory();
	CommonFunctions.waitForElementVisible(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]",
			getDriver());
	channelsPage.buttonChannel.click();
	CommonFunctions.waitForElementVisible(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ImageButton",
			getDriver());
	channelsPage.buttonPencil.click();
	CommonFunctions.waitForElementVisible(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.EditText",
			getDriver());
	channelsPage.textAreaEditPost.sendKeys(randomString);
	channelsPage.buttonSend.click();
	Thread.sleep(5000);
	CommonFunctions.scrollToElementUsingText(getDriver(), randomString);
		Assert.assertEquals(channelsPage.textPostNames.getText(), randomString);
		channelsPage.buttonPostEditMenus.click();
		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, getDriver()).getText(),
				Constants.DELETE_POST);
		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.EDIT_POST, getDriver()).getText(),
				Constants.EDIT_POST);
		CommonFunctions.selectElementUsingText(Constants.EDIT_POST, getDriver()).click();
		Assert.assertEquals(channelsPage.headerEditPost.getText(), Constants.EDIT_POST);
		channelsPage.textAreaEditPost.sendKeys(randomString1);
		System.out.println(channelsPage.textAreaEditPost.isEnabled());
		channelsPage.buttonSend.click();
		Thread.sleep(6000);
		getDriver().navigate().back();
		channelsPage.buttonChannel.click();
		CommonFunctions.scrollToElementUsingText(getDriver(), randomString1);
		Assert.assertEquals(channelsPage.textPostNames.getText(), randomString1);

	}
	
	
	@Test(priority = 3)
	public void ManagerCanPinC14() throws FileNotFoundException, InterruptedException {
	LoginPage loginPage = new LoginPage();
	String randomString = CommonFunctions.generateRandomString(8);
	String randomString1 = CommonFunctions.generateRandomString(10);
	loginPage.getLoginFunctionality(prop.getProperty("AdminUername"), prop.getProperty("AdminPassword"));
	HomePage homePage = new HomePage();
	Thread.sleep(5000);
	homePage.buttonSideMenu.click();
	CommonFunctions.selectElementUsingText(Constants.CHANNELS, getDriver()).click();
	Thread.sleep(6000);
	ChannelsPageFactory channelsPage = new ChannelsPageFactory();
	CommonFunctions.waitForElementVisible(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]",
			getDriver());
	channelsPage.buttonChannel.click();
	CommonFunctions.waitForElementVisible(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageView[2]",
			getDriver());
	channelsPage.buttonPostEditMenus.click();
	Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.EDIT_POST, getDriver()).getText(),
			Constants.PIN_POST);
	Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, getDriver()).getText(),
			Constants.DELETE_POST);
	Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.EDIT_POST, getDriver()).getText(),
			Constants.EDIT_POST);
	CommonFunctions.selectElementUsingText(Constants.PIN_POST, getDriver()).click();
	Assert.assertEquals(channelsPage.headerEditPost.getText(), Constants.PIN_POST);
	
	}
	
	@Test(priority = 3)
	public void ManagerCanPinC15() throws FileNotFoundException, InterruptedException {
	LoginPage loginPage = new LoginPage();
	String randomString = CommonFunctions.generateRandomString(8);
	String randomString1 = CommonFunctions.generateRandomString(10);
	loginPage.getLoginFunctionality(prop.getProperty("AdminUername"), prop.getProperty("AdminPassword"));
	HomePage homePage = new HomePage();
	Thread.sleep(5000);
	homePage.buttonSideMenu.click();
	CommonFunctions.selectElementUsingText(Constants.CHANNELS, getDriver()).click();
	Thread.sleep(6000);
	ChannelsPageFactory channelsPage = new ChannelsPageFactory();
	CommonFunctions.waitForElementVisible(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]",
			getDriver());
	channelsPage.buttonChannel.click();
	CommonFunctions.waitForElementVisible(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageView[2]",
			getDriver());
	channelsPage.buttonPostEditMenus.click();
	Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.EDIT_POST, getDriver()).getText(),
			Constants.PIN_POST);
	Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, getDriver()).getText(),
			Constants.DELETE_POST);
	Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.EDIT_POST, getDriver()).getText(),
			Constants.EDIT_POST);
	CommonFunctions.selectElementUsingText(Constants.PIN_POST, getDriver()).click();
	Assert.assertEquals(channelsPage.headerEditPost.getText(), Constants.PIN_POST);
	
	}

}

