package demo.co.Base;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

/**
 * @author Ranjit
 */
public class CommonFunctions {
	TimeZone timeZone = TimeZone.getTimeZone("Canada/Eastern");
	Calendar c = GregorianCalendar.getInstance();
	
	public static Properties getProperty(String propertyFilePath) throws FileNotFoundException {
		BufferedReader reader = null;
		reader = new BufferedReader(new FileReader(propertyFilePath));
		Properties properties = new Properties();
		try {
			properties.load(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties;

	}
	
	public static void waitForElementVisible(final String ele,RemoteWebDriver driver)
	{
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
			    .withTimeout(30, TimeUnit.SECONDS)
			    .pollingEvery(5, TimeUnit.SECONDS)
			    .ignoring(NoSuchElementException.class);

			WebElement foo = wait.until(new Function<WebDriver, WebElement>() 
			{
			  public WebElement apply(WebDriver driver) {
			  return driver.findElement(By.xpath(ele));
			}
			});
	}


	public static String generateRandomString(int n) {
		String randomString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int) (randomString.length() * Math.random());
			sb.append(randomString.charAt(index));
		}
		return sb.toString();
	}

	public static String getScreenShot(RemoteWebDriver driver) {
		SimpleDateFormat df = new SimpleDateFormat("_d-MM-yyyy_h_mm_a");
	    df.setTimeZone(TimeZone.getTimeZone("Canada/Eastern"));
	    
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String screenShotPath = System.getProperty("user.dir") + "/screenshot/"+"WorkJam"+df.format(new Date())+".png";
		try {
			FileUtils.copyFile(src, new File(screenShotPath));
		} catch (IOException e) {
			System.out.println("captured failed" + e.getMessage());
		}
		return screenShotPath;
	}

	public static WebElement selectElementUsingText(String menu, RemoteWebDriver driver) {
		WebElement element = driver.findElementByXPath(String.format("//*[@text='%s']", menu));
		return element;

	}

	
	public static void scrollToElementUsingText(RemoteWebDriver driver,String visibleText)
	{
		try {
			((AndroidDriver<MobileElement>)driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textStartsWith(\""+visibleText+"\"))");
			}catch(Exception e) {}
	}




	public static void scrollToElementUsingText(AndroidDriver<AndroidElement> driver, String visibleText) {
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text(\""
						+ visibleText + "\").instance(0))")
				.getText();

	}

	public static String getCurrentWeek(RemoteWebDriver driver) {
		
		Calendar c = GregorianCalendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

		DateFormat df = new SimpleDateFormat("E, MMM d", Locale.getDefault());
		df.setTimeZone(TimeZone.getTimeZone("Canada/Eastern"));
		String startDate = "", endDate = "";

		startDate = df.format(c.getTime());
		c.add(Calendar.DATE, 6);
		endDate = df.format(c.getTime());
		return startDate;

	}

	public static String getCurrentDate(RemoteWebDriver driver) {
		
		Calendar c = GregorianCalendar.getInstance();
		
		
		DateFormat df = new SimpleDateFormat("EEEE, MMMM d", Locale.getDefault());
		df.setTimeZone(TimeZone.getTimeZone("Canada/Eastern"));
		return df.format(c.getTime());

	}
	public static String getCurrentDatePlus2Date(RemoteWebDriver driver) {
		
		Calendar c = GregorianCalendar.getInstance();
		c.add( Calendar.DATE,2);

		DateFormat df = new SimpleDateFormat("EEEE, MMMM d", Locale.getDefault());
		df.setTimeZone(TimeZone.getTimeZone("Canada/Eastern"));
		return df.format(c.getTime());

	}
	public static void getScreenShotAll(RemoteWebDriver driver,String path) {
		
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String screenShotPath = path+".png";
		try {
			FileUtils.copyFile(src, new File(screenShotPath));
		} catch (IOException e) {
			System.out.println("captured failed" + e.getMessage());
		}
		
	}
}
