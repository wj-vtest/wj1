package demo.co.Base;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
/**
 * @author Bobby
 */
public class slackUtils {
	private static String slackWebhookUrl = "https://hooks.slack.com/services/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";

	public static void sendMessage(slackMessage message) {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(slackWebhookUrl);
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String json = objectMapper.writeValueAsString(message);
			StringEntity entity = new StringEntity(json);
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");
			client.execute(httpPost);
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}