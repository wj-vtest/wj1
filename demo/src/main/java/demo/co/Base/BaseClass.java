/*
 * Project Name : demo Demo Framework
* Author : vTESTcorp
* Version : V1.0.demo
* Reviewed By : Manjeet
* Date of Creation : Feb 7, 2019
* Modification History :
* Date of change : Feb 15, 2019
* Version : V1.1.demo
* changed function : 
* change description :
* Modified By : Manjeet
*/

package demo.co.Base;

import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.google.common.io.Files;

//Handling Config file operations and Extent report initialization
//Included @BeforeSuite & @AfterSuite capabilities for handling various iOS platforms

public class BaseClass {

	public static ThreadLocal<RemoteWebDriver> TLD = new ThreadLocal<RemoteWebDriver>();

	public static Properties prop; // Property file initialization
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public ExtentTest logger;
	public static String Report_Path = null;
	public static String platform_version;
	public String atest = "";
	public String folder, basefold;

	public BaseClass() {
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream("src//main//java//demo//co//Config//config.properties");
			prop.load(ip);
		} catch (Exception ex) {
			System.out.println(ex.getStackTrace());
		}
	}

	@BeforeSuite
	public void setUp() {
		try {
			SimpleDateFormat df = new SimpleDateFormat("_d-M-yyyy_h-mm-ss");
			df.setTimeZone(TimeZone.getTimeZone("Canada/Eastern"));
			// Initialize Extent Report required static fields.
			folder = df.format(new Date());
			basefold = System.getProperty("user.dir") + "\\screenshot\\" + folder + "\\";
			System.out.println(System.getProperty("user.dir"));

			String Report_Timestamp = "Extent_Report" + (LocalDateTime.now()).getHour() + "_"
					+ (LocalDateTime.now()).getMinute() + "_" + (LocalDateTime.now()).getSecond();

			Report_Path = System.getProperty("user.dir") + "//test-output//Report//" + Report_Timestamp;
			File f1 = new File(Report_Path);
			f1.mkdir();
			File F2 = new File(Report_Path + "//" + "Snapshot");
			F2.mkdir();

			htmlReporter = new ExtentHtmlReporter(Report_Path + "//STMExtentReport" + "_" + Report_Timestamp + ".html");
			extent = new ExtentReports();
			extent.attachReporter(htmlReporter);
			extent.setSystemInfo("Host Name", "Automation Server");
			extent.setSystemInfo("Environment", "Windows-Appium-iOS");
			extent.setSystemInfo("User Name", "vTest QA Team");

			htmlReporter.config().setDocumentTitle("iOS-Appium execution report");
			htmlReporter.config().setReportName("iOS-Appium Execution Report");
			htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
			htmlReporter.config().setTheme(Theme.STANDARD);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

	/*
	 * Initializing pre-requisite capabilities necessary for invoking corresponding
	 * device.
	 */
	@BeforeMethod
	@Parameters({ "type", "APPIUM_SERVER_URL", "platformName", "deviceName", "appPackage", "appActivity",
			"platformVersion" })
	public void beforeTest(String type, String APPIUM_SERVER_URL, String platformName, String deviceName,
			String appPackage, String appActivity, String platformVersion) throws Exception {
		RemoteWebDriver driver = null;

		URL url = new URL(APPIUM_SERVER_URL);

		DesiredCapabilities caps = new DesiredCapabilities();

		if (type.equalsIgnoreCase("Android")) {
			caps.setCapability("deviceName", deviceName);
			caps.setCapability("platformName", platformName);
			platform_version = deviceName;
			caps.setCapability("appPackage", appPackage);
			caps.setCapability("appActivity", appActivity);
			caps.setCapability("platformVersion", platformVersion);
			caps.setCapability("testobject_api_key", "774249378DA948BD9014EB5C2491EF9E");
			caps.setCapability("appiumVersion", "1.15.1");
			caps.setCapability("testobject_app_id", "1");

		} else {
			caps.setCapability("automationName", prop.getProperty("automationName"));
			caps.setCapability("deviceName", deviceName);
			platform_version = deviceName;
			caps.setCapability("udid", platformVersion);
			caps.setCapability("platformName", prop.getProperty("platformName"));
		}

		driver = new RemoteWebDriver(url, caps);
		setDriver(driver);

		getDriver().manage().timeouts().implicitlyWait(Integer.parseInt(prop.getProperty("object_wait_timeout")),
				TimeUnit.SECONDS);

	}

	public void setDriver(RemoteWebDriver driver) {
		TLD.set(driver);
	}

	public RemoteWebDriver getDriver() {
		return TLD.get();
	}

	public static String getScreenshot(RemoteWebDriver appiumDriver, String screenshotName) throws Exception {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) appiumDriver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destination = Report_Path + "//Snapshot//" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		Files.copy(source, finalDestination);
		return destination;
	}

	@AfterMethod
	public void teardown(ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {
			logger.log(Status.FAIL,
					MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
			logger.log(Status.FAIL,
					MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
			Thread.sleep(2000);
			String screenshotPath = BaseClass.getScreenshot(getDriver(), result.getName());
			logger.log(Status.FAIL, "Screenshot from : " + screenshotPath,
					MediaEntityBuilder.createScreenCaptureFromPath(screenshotPath).build());
			atest += result.getTestClass().getName() + "." + result.getName() + " - Failed\n";
			CommonFunctions.getScreenShotAll(getDriver(), basefold + result.getName() + "_fail");
		} else if (result.getStatus() == ITestResult.SKIP) {
			logger.log(Status.SKIP,
					MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));
			atest += result.getTestClass().getName() + "." + result.getName() + " - skipped\n";

		} else {
			atest += result.getTestClass().getName() + "." + result.getName() + " - Passed\n";
			CommonFunctions.getScreenShotAll(getDriver(), basefold + result.getName() + "_pass");
		}
		getDriver().quit();

	}

	@AfterTest
	public void afterTest() {
		getDriver().quit();
		TLD.set(null);
	}

	@AfterSuite
	public void tearDown() throws IOException {
		SendMailForFailedScenarios.SendMail(folder, atest);

		slackMessage slackMsg = slackMessage.builder().username("user")
				.text("-----------------Test status------------------\n"
						+ "| No | PackageName.className.TestCaseName | status |\n" + atest
						+ "---------------------------------------------")
				.icon_emoji(":twice:").build();
		slackUtils.sendMessage(slackMsg);

		extent.flush();
	}
}
